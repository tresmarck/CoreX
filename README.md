# Project Alloy
Project Alloy is a 2D top-down video game where one will play as the hacker, and the other will play as the agent.

### Development

Project Alloy will first be released on Linux, and then Windows. Further releases will focus on macOS, FreeBSD, and consoles (Xbox One, PlayStation 4, and maybe others as well).

To start developing, please clone this repository unto your machine using Git. After cloning, switch to the `develop` branch and begin branching out to start on a feature.

The recommended compiler to be used is Clang.

### Note

The information in this `README` is inadequate. Furthermore, future details and information will be moved to the Wiki in the future.
